#!/bin/bash

set -e

REMOVE_PACKAGES=(
        'libreoffice*'              # Libreoffice suite
        'libreport'                 # Bug auto reporter
        'gnome-calendar*'           # Calendar
        'gnome-contacts*'           # Contact
        'gnome-maps*'               # Maps
        'gnome-photos*'             # Photos
        'gnome-remote-desktop*'     # Remote desktop feature
        'gnome-software*'           # GUI app to manage software
        'rhythmbox*'                # Default music player2>2                
)