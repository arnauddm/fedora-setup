#!/bin/bash

PACKAGES=(
        'gnome-shell-extension-user-theme'
        'gnome-shell-extension-dash-to-dock'
        'arc-theme'
        'breeze-cursor-theme'                 # 3d printing slicer
)

[[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }

# Set wallpaper
echo "Copying wallpaper to default Pictures folder"
mkdir -p $HOME/Pictures/Wallpapers
cp -f files/wallpaper.jpg $HOME/Pictures/Wallpapers/.

# Set icon theme
echo "Installing: Tela icons theme"
if [ ! -d ~/tools/Tela-icon-theme ]
then
    (cd ~/tools && git clone --single-branch --branch master https://github.com/vinceliuice/Tela-icon-theme.git && cd Tela-icon-theme && ./install.sh)
fi

# Gogh shell theme
echo 3 | bash -c  "export TERMINAL=gnome-terminal; $(wget -qO- https://git.io/vQgMr)" Unnamed