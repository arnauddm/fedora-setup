#!/bin/bash

set -e

PACKAGES=(
        'filezilla'               # File transfer
        'transmission'            # Torrent utility
        'wireshark'               # Network analyser
)
