#!/bin/sh

set -e

PACKAGES=(
        'timeshift'               # Backup tool    
        'homebank'                # Accounting tool    
        'ulauncher'               # App launcher (Ctrl + Space)
)