#!/bin/bash

# Outputs JSON to i3bar
# Inspired by https://github.com/Joe-Dowd/dots/blob/master/.i3/status.sh

# Color defs
WHITE=#ffffff
BLACK=#000000

# Global variables
update_available=""

# $1 : Name
# $2 : Text
# $3 : Text color
# $4 : Background color
echo_new_item() {
    echo '{'
      echo "\"name\":\"$1\","
      echo "\"color\": \"$3\","
      echo "\"background\":\"$4\","
      echo "\"full_text\":\" $2 \""
    echo "}"
}

echo_separator() {
  echo ","
}

echo_spotify() {
    status=`dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'PlaybackStatus'|egrep -A 1 "string"|cut -b 26-|cut -d '"' -f 1|egrep -v ^$`

    if [ $? -eq 0 ]
    then
      artist=`dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'Metadata'|egrep -A 2 "artist"|egrep -v "artist"|egrep -v "array"|cut -b 27-|cut -d '"' -f 1|egrep -v ^$`
      album=`dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'Metadata'|egrep -A 1 "album"|egrep -v "album"|cut -b 44-|cut -d '"' -f 1|egrep -v ^$`
      title=`dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'Metadata'|egrep -A 1 "title"|egrep -v "title"|cut -b 44-|cut -d '"' -f 1|egrep -v ^$`
      
      echo_new_item "spotify" " $title by $artist" $BLACK "#1ed760"
    else
      echo_new_item "spotify" " Not started" $BLACK "#1ed760"
    fi
}

echo_cpu_usage() {
  local cpu=$(echo "`LC_ALL=C top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1}'`")
  cpu_str=$cpu
  local len=${#cpu}

  if [ $len -eq 3]
  then
    cpu_str="0$cpu"
  fi

  local cpu_temp=$(sensors | grep Tctl | cut -d':' -f2 | cut -d ' ' -f10)

  echo_new_item "cpu_usage" " $cpu_str % |  $cpu_temp" $WHITE "#845ec2"
}

echo_volume_level() {
  local volume_status=$(awk -F"[][]" '/Left:/ { print $4 }' <(amixer sget Master))

  local volume=0
  if [ "$volume_status" == "on" ]
  then
    volume=$(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')
  fi

  local volume_bar=""
  local bounded_volume=$volume
  if [[ $volume -gt 100 ]]
  then
    bounded_volume=100
  fi
  for i in `seq 1 $( expr $bounded_volume / 10 )`
  do
    volume_bar+=""
  done
  for i in `seq $( expr $bounded_volume / 10 ) 9`
  do
    volume_bar+=" "
  done

  echo_new_item "volume_level" " [$volume_bar] ($volume%)" $WHITE "#d65db1"
}

echo_date() {
  local date_time=$(echo "$(date '+%d') $(date '+%b') $(date '+%y') $(date '+%T')")
  echo_new_item "datetime" " $date_time" $WHITE "#ff6f91"
}

echo_available_update() {
  if [ "$update_available" == "" ]
  then
    update_available=$(dnf check-update | grep updates | wc -l) 
  fi

  if [ $update_available -eq 0 ]
  then
    echo_new_item "updates" "  No updates" $WHITE "#ff9671"
  else
    echo_new_item "updates" " ${update_available} updates" $WHITE "#ff9671"
  fi

}

echo_current_network_speed() {
  local all_speed=$(awk '/enp3s0/ {i++; rx[i]=$2; tx[i]=$10}; END{print rx[2]-rx[1] " " tx[2]-tx[1]}' \
 <(cat /proc/net/dev; sleep 1; cat /proc/net/dev))
  local download=$(echo $all_speed | cut -d' ' -f1)
  local upload=$(echo $all_speed | cut -d' ' -f2)

  echo_new_item "speed" " $download |  $upload" $WHITE "#00b3c3"
}

echo_local_ip() {
  local ip=$(ifconfig | grep enp3s0 --after-context=1 | head -n 2 | tail -n 1 | cut -d' ' -f10)
  echo_new_item "local_ip" " $ip" $WHITE "#FF6F91"
}

echo_unread_mail() {
  local unread=$(find ~/.thunderbird/*/ImapMail -name 'INBOX*.msf' -exec sh -c "grep '(^A2=' '{}' | tail -n1 | sed -r 's/.*\(\^A2=(\w+)\).*/\1/' | xargs -n1 -L1 --replace=__ printf '%d\n' '0x__'" \;)

  if [ $unread -eq 0 ]
  then
    echo_new_item "unread_mail" "  No unread email" $WHITE "#FF6F91"
  else
    echo_new_item "unread_mail" "  Unread : $unread" $WHITE "#FF6F91"
  fi
}


# Init i3 bar "headers"
echo '{ "version": 1, "click_events": true }'
echo '['
echo '[]'

#
# Send blocks with info forever
#
while :; do
  echo ",["
    echo_cpu_usage
    echo_separator

    echo_local_ip
    echo_separator

    echo_current_network_speed
    echo_separator

    echo_available_update
    echo_separator

    echo_unread_mail
    echo_separator

    echo_spotify
    echo_separator

    echo_volume_level
    echo_separator

    echo_date
  echo "]"
  usleep 100
done
