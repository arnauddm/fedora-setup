#!/bin/bash

set -e

[[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }

if [ $# -ne 1 ]
then
    sudo_cmd="sudo"
else
    sudo_cmd="echo $1 | sudo -S -k"
fi

echo "Installing needed packages"
eval $sudo_cmd dnf install -y i3 fontawesome-fonts.noarch reofi rofi-theme

echo "Applying custom theme"
cp -rf i3/* ~/.config/i3/.
cp -rf i3status/* ~/.config/i3status/.
cp -rf rofi/* ~/.config/rofi/.

echo "Now you need to logout or reboot your system to apply all of these changes. Be sure to select 'i3' window manager on login screen"