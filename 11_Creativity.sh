#!/bin/sh

set -e

PACKAGES=(
        'kdenlive'               # Video maker
        'gimp'                   # Image maker 
        'freecad'                # CAO design
        'slic3r'                 # 3d printing slicer
)