#!/bin/bash

set -e

PACKAGES=(
        'golang'                # Go language
        'qt5-qtbase-devel'      # Qt Framework
        'qt5-qtquickcontrols2-devel' # Qt Quick
        'htop'                  # System monitor
        'docker-ce'             # Docker system
        'docker-compose'        # Docker "image builder"
        'doxygen'		        # C++ documentation
        'plantuml'		        # Class diagram
        'lcov'			        # C++ Code coverage tool
        'java-11-openjdk'	    # Java 11 JDK
        'vim'
	'fish'
)

GROUPS=(
        "Development Tools"               # Compiler, ...
)

[[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }

if [ $# -ne 1 ]
then
    sudo_cmd="sudo"
else
    sudo_cmd="echo $1 | sudo -S -k"
fi

# Bash it theme
echo "Cloning: Bash-it"
if [ ! -d ~/tools/bash-it ]
then
    (cd ~/tools && git clone https://github.com/Bash-it/bash-it && cd bash-it && ./install.sh --silent -f && sed -i "s/bobby/sexy/g" ~/.bashrc)
fi

# Docker configuration
echo "Configuring Docker"
eval $sudo_cmd usermod -aG docker $(whoami)

# Git configuration
echo "Configuring Git"
git config --global core.editor vim
git config --global diff.tool vimdiff

# Installing and initializing Flutter
if [ ! -d ~/tools/flutter ]
then
    (cd ~/tools && git clone https://github.com/flutter/flutter.git -b stable && echo "export PATH=\"$PATH:~/tools/flutter/bin\"" >> ~/.bashrc)
    ~/tools/flutter/bin/flutter doctor
    ~/tools/flutter/bin/flutter config --enable-linux-desktop
fi

# Installing fish tools (fisher)
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
fisher install IlanCosman/tide
