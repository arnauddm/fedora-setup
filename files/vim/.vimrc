if &diff
  " colorscheme evening
  highlight DiffAdd    cterm=bold ctermfg=238 ctermbg=205 gui=none guifg=bg guibg=Red
  highlight DiffDelete cterm=bold ctermfg=238 ctermbg=71 gui=none guifg=bg guibg=Red
  highlight DiffChange cterm=bold ctermfg=238 ctermbg=214 gui=none guifg=bg guibg=Red
  highlight DiffText   cterm=bold ctermfg=238 ctermbg=214 gui=none guifg=bg guibg=Red
endif
