#!/bin/bash

set -e

PACKAGES=(
    'vim'               # Vim editor
    'code'              # Visual Studio Code
    'qt5-designer'	# Qt5 Application designer
    'terminator'
)

VS_CODE_EXTENSIONS=(
    'ms-vscode.cpptools'            # C / C++
    'ms-azuretools.vscode-docker'   # Docker
    'cschlosser.doxdocgen'          # Doxygen Documentation Generator
    'dracula-theme.theme-dracula'   # Dracula Official
    'dcasella.i3'                   # i3
    'pkief.material-icon-theme'     # Material Icon Theme
    'jebbs.plantuml'                # PlantUML
    '2gua.rainbow-brackets'         # Rainbow Brackets
)

[[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }

if [ ! -d ~/tools/idea* ]
then
    echo "Downloading: Intellij"
    wget https://download.jetbrains.com/idea/ideaIC-2020.1.1.tar.gz 2>&1 > /dev/null
    echo "Decompressing: Intellij"
    tar xvzf idea*.tar.gz 2>&1 > /dev/null
    rm -rf idea*.tar.gz
    mv idea* $HOME/tools
    echo "Creating shortcut"
    IDEA_NAME=$(ls $HOME/tools | grep idea)
    #sed "s/##IDEA_PATH##/$USER\/tools\/$IDEA_NAME/g" files/jetbrains-idea-ce.desktop >> "$HOME/.local/share/applications/intellij.desktop"   
    sed -i "s/##USER##/$USER/g" files/jetbrains-idea-ce.desktop
    sed -i "s/##IDEA_FOLDER##/$IDEA_NAME/g" files/jetbrains-idea-ce.desktop
    cp files/jetbrains-idea-ce.desktop $HOME/.local/share/applications/intellij.desktop
fi


echo
echo "Done!"
echo
