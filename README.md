# Fedora setup
This project is composed of many bash scripts to initialize and configure a [fedora](https://getfedora.org) image.

# Results
<img src="docs/home.png" width="50%"/><img src="docs/notification_popup.png" width="50%"/>
<img src="docs/apps_menu.png" width="50%"/><img src="docs/poweroff_menu.png" width="50%"/>

# Requirements
All you need is this project, nothing else !
To get it, 2 solutions :

* `git clone https://gitlab.com/arnauddm/fedora-setup.git`
* Download zip

# How this repo works ?
You can find out differents branches and particularly these ones :

* [master](https://gitlab.com/arnauddm/fedora-setup/-/tree/master) : Stable and all scripts are tested before merging into this branch
* [develop](https://gitlab.com/arnauddm/fedora-setup/-/tree/develop) : Unstable version and with current modifications and some scripts can contain bugs

# Executing scripts
To execute scripts, you can use, for example, the following command :
```
./00_Update.sh
```

**Important : After running the script `09_GnomeCustomization_Part1.sh`, you must reboot your machine before executing the (next) script `09_GnomeCustomization_Part2.sh` (some customization "items" need a restart to be available...).**

# What will be installed
As packages are organized by category, you are free to execute (install) only what you want / need. If a category installs an unwanted package, you can comment the line by putting '#' in front of the line.

## [01_SetupMirrors](01_SetupMirrors.sh)
Enabling differents repositories :

- [RPM Fusion](https://rpmfusion.org)
- [Visual Studio](https://code.visualstudio.com) Repository
- [Flathub](http://flathub.org) : Flatpak repository


## [02_BasicSoftware](02_BasicSoftware.sh)

- [Firefox](https://www.mozilla.org/fr/firefox/) : Web browser
- [WPS Office](https://www.wps.com) : Alternative (Open-Source) to Microsoft Office
- [Thunderbird](https://www.thunderbird.net/fr/) : Email client


## [03_Multimedia](03_Multimedia.sh)

- [VLC](https://www.videolan.org/vlc/index.fr.html) : Media Player
- [Spotify](http://spotify.com) : Online library

## [04_Network](04_Network.sh)

- [FileZilla](https://filezilla-project.org) : File transfer tool
- [Transmission](https://transmissionbt.com) : Torrent client

## [05_CodingTools](05_CodingTools.sh)

- [Go](https://golang.org) : Programming language
- [Qt5](https://www.qt.io) : C++ framework
- Development tools (compiler, ...)
- [Bash-it](https://github.com/Bash-it/bash-it) : Bash theme
- htop : system monitor
- [Docker](https://www.docker.com/) : Container manager
- [Docker compose](https://docs.docker.com/compose/) : Build Docker application with multiple containers

## [06_CodingSoftware](06_CodingSoftware.sh)

- [VIM](https://www.vim.org) : Text editor
- [Visual Studio Code](https://code.visualstudio.com) : Text editor
- [Terminator](https://gnometerminator.blogspot.com/p/introduction.html) : Terminal tools (to split your terminal...)
- [Intellij Community Edition](https://www.jetbrains.com/fr-fr/idea/) : IDE
- [Qt5 Designer](https://doc.qt.io/qt-5/qtdesigner-manual.html) : GUI Designer for Qt Framework

## [07_Drivers](07_Drivers.sh)

- Fuse Exfat : USB driver for Fat32 & exFat format

## [08_Social](08_Social.sh)

- [Discord](https://discordapp.com) : Chat client

## [09_CleanUnwantedSoftware](09_CleanUnwantedSoftware.sh)
** Removing these packages : **

- [LibreOffice](https://libreoffice.org) : Suite office
- [Libreport](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/6.3_technical_notes/abrt_and_libreport) : Bug reporter
- [gnome-boxes](https://wiki.gnome.org/Apps/Boxes) : Virtualization tool
- [gnome-calendar](https://wiki.gnome.org/Apps/Calendar) : Default calendar app
- [gnome-contacts](https://wiki.gnome.org/Apps/Contacts) : Default contacts app
- [gnome-maps](https://wiki.gnome.org/Apps/Maps) : Default map app
- [gnome-photos](https://wiki.gnome.org/Apps/Photos) : Default photo viewer
- [gnome-remote-desktop](https://wiki.gnome.org/Projects/Mutter/RemoteDesktop) : Default remote desktop tool
- [gnome-software](https://wiki.gnome.org/Apps/Software) : Graphical package manager
- [rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) : Default music player
 
## [10_Tools](10_Tools.sh)

- Timeshift : Backup tool
- [HomeBank](http://homebank.free.fr/en/index.php) : Accounting tool


## [99_GnomeCustomization](99_GnomeCustomization_Part1.sh)

- [user-theme](https://extensions.gnome.org/extension/19/user-themes/) extension
- [Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/)
- [Tela](https://github.com/vinceliuice/Tela-icon-theme) icon theme
- [Arc](https://github.com/horst3180/arc-theme) theme
- [Breeze cursor](https://www.gnome-look.org/p/999927/) 

> The links provided in these lists are not necessarily the ones I used

# Latest modifications
### 09 May 2020

* Adding Timeshift


### 08 May 2020

* Adding Bash-it theme
* Refactoring differents scripts

### 06 May 2020

* Enabling Flathub repository (Flatpak)
* Adding Spotify
* Fixing Intellij icon to be automatically changed when switching icons theme

### 05 May 2020

* First version
