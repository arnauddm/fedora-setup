#!/bin/bash


PACKAGES=(
        'firefox'               # Firefox web browser
        'evolution'             # Email client
        'evolution-ews'         # Microsoft Exchange / Outlook plugin for mail client
        'util-linux-user' # chsh
)

FLATPAK=(
    'com.wps.Office'
)