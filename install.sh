#!/bin/bash

set -e

export PKG_TO_INSTALL=""
export PKG_TO_REMOVE=""
export SNAP_TO_INSTALL=""
export PKG_GROUP_TO_INSTALL=""
export VS_CODE_EXTENSIONS_TO_INSTALL=""
export FLATPAK_TO_INSTALL=""

read -s -p 'Enter your password to continue:' pswd
SUDO="echo $pswd | sudo -S -k"

eval $SUDO ./tearup.sh

# First : retrieve all packages to install
for i in {0..90}
do
   EXE_NUMBER="${i}"
   
   # Use 2 digit
   if [ $i -lt 10 ]
   then
      EXE_NUMBER="0${i}"
   fi

   # Execute the script if exists
   COUNT=$(ls -l | grep "${EXE_NUMBER}_" | wc -l)
   if [ $COUNT -eq 1 ]
   then
      unset PACKAGES
      unset FLATPAK
      unset GROUPS
      unset VS_CODE_EXTENSIONS
      unset REMOVE_PACKAGES

      source ${EXE_NUMBER}_*.sh
      
      if [ -n "$PACKAGES" ]
      then
         for PKG in "${PACKAGES[@]}"; do
            PKG_TO_INSTALL="$PKG_TO_INSTALL $PKG"
         done
      fi

      if [ -n "$FLATPAK" ]
      then
         for PKG in "${FLATPAK[@]}"; do
            FLATPAK_TO_INSTALL="$FLATPAK_TO_INSTALL $PKG"
         done
      fi

      if [ -n "$GROUPS" ]
      then
         for PKG in "${GROUPS[@]}"; do
            PKG_GROUP_TO_INSTALL="$PKG_GROUP_TO_INSTALL $PKG"
         done
      fi

      if [ -n "$VS_CODE_EXTENSIONS" ]
      then
         for PKG in "${VS_CODE_EXTENSIONS[@]}"; do
            VS_CODE_EXTENSIONS_TO_INSTALL="$VS_CODE_EXTENSIONS_TO_INSTALL $PKG"
         done
      fi

      if [ -n "$REMOVE_PACKAGES" ]
      then
         for PKG in "${REMOVE_PACKAGES[@]}"; do
            PKG_TO_REMOVE="$PKG_TO_REMOVE $PKG"
         done
      fi
   fi

done

unset PACKAGES
source 99_*_Part1.sh
      
if [ -n "$PACKAGES" ]
then
   for PKG in "${PACKAGES[@]}"; do
      PKG_TO_INSTALL="$PKG_TO_INSTALL $PKG"
   done
fi


eval $SUDO dnf update -y --best --allowerasing
# sudo dnf group install -y "$PKG_GROUP_TO_INSTALL"
eval $SUDO dnf install -y $PKG_TO_INSTALL
eval $SUDO flatpak install -y flathub $FLATPAK_TO_INSTALL
# eval $SUDO snap install $SNAP_TO_INSTALL
code --install-extension  $VS_CODE_EXTENSIONS_TO_INSTALL
eval $SUDO dnf remove -y $PKG_TO_REMOVE

# Then, proceed the installation and configuration
for i in {0..90}
do
   EXE_NUMBER="${i}"
   
   # Use 2 digit
   if [ $i -lt 10 ]
   then
      EXE_NUMBER="0${i}"
   fi

   # Execute the script if exists
   COUNT=$(ls | grep "${EXE_NUMBER}_" | wc -l)
   if [ $COUNT -eq 1 ]
   then
      echo "RUNNING ${EXE_NUMBER}_*.sh"
      ./${EXE_NUMBER}_*.sh $pswd
   fi

done

./99_*_Part1.sh

echo "Please reboot your machine before executing the script : 99_GnomeCustomization_Part2.sh"