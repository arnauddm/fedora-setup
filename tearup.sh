#!/bin/bash

set -e

echo "Creating 'tools' repository"
cd $HOME
mkdir -p tools

if [ $# -ne 1 ]
then
    sudo_cmd="sudo"
else
    sudo_cmd="echo $1 | sudo -S -k"
fi

eval $sudo_cmd dnf -y install flatpak snapd
eval $sudo_cmd rm -f /snap/snap
eval $sudo_cmd ln -s /var/lib/snapd/snap /snap

# Install and enable RPM Fusion
echo "Enabling RPM Fusio repository"
eval $sudo_cmd dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
eval $sudo_cmd dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Visual Studio Code
echo "Enabling repositories for the further installation of Visual Studio Code"
eval $sudo_cmd rpm --import https://packages.microsoft.com/keys/microsoft.asc 
echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" | eval $sudo_cmd tee -a /etc/yum.repos.d/vscode.repo

echo "Enablig: Flathub repository (Flatpak)"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "Updating all repositories after these modifications"
eval $sudo_cmd dnf update -y 

echo "Fetching docker-ce repository"
eval $sudo_cmd dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo