#!/bin/bash

set -e

[[ "${#BASH_SOURCE[@]}" -gt "1" ]] && { return 0; }

# User theme
echo "Enabling: user-theme"
gnome-extensions enable $(gnome-extensions list | grep user-theme)

# Dash to Dock
echo "Enabling: dash-to-dock"
gnome-extensions enable $(gnome-extensions list | grep dash-to-dock)

# Dask to Dock : bottom position
echo "Setting: Dash-to-dock: BOTTOM"
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position "BOTTOM"

# Fedora's logo on wallpaper
echo "Disabling: background-logo"
gnome-extensions disable $(gnome-extensions list | grep background-logo)

# Places menu
echo "Disabling: places-menu"
gnome-extensions disable $(gnome-extensions list | grep places-menu)

# Places menu
echo "Enabling: Applications menu"
gnome-extensions disable $(gnome-extensions list | grep apps-menu)

# Set wallpaper
echo "Applying wallpaper"
gsettings set org.gnome.desktop.background picture-uri "/home/$USER/Pictures/Wallpapers/wallpaper.jpg"

# Set icon theme
echo "Enabling: Tela icons theme"
gsettings set org.gnome.desktop.interface icon-theme 'Tela'

# Set main theme
echo "Enabling: arc-theme (dark version)"
gsettings set org.gnome.shell.extensions.user-theme name "Arc-Dark"
gsettings set org.gnome.desktop.interface gtk-theme "Arc-Dark"

# set cursor theme
echo "Enabling: breeze-cursor-theme"
gsettings set org.gnome.desktop.interface cursor-theme 'breeze_cursors'

# Window title bar
echo "Enabling: Minimize & Maximize window buttons"
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'

# Apply terminator config
mkdir -p ~/.config/terminator
cp files/terminator/config ~/.config/terminator/.

# Apply vim config
cat files/vim/.vimrc >> ~/.vimrc